# A bucket to store the code for rebellion lambda

resource "aws_s3_bucket" "rebel-yell" {
    bucket = "rebel-yell-poc-foo"
}

resource "aws_s3_bucket_acl" "priv-rebel-yell" {
    bucket = aws_s3_bucket.rebel-yell.bucket
    acl    = "private"
}

resource "aws_iam_policy" "lambda-cw-logger" {
    name = "lambda_logging"
    path = "/"
    description = "Log lambda to cw"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:*",
            "Effect": "Allow"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda-attachment" {
    role = aws_iam_role.iam_lambda.name
    policy_arn = aws_iam_policy.lambda-cw-logger.arn
}

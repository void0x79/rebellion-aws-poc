# Monitoring Policies (administration level)

# Admin access to everything cloudwatch
resource "aws_iam_group_policy" "monitoring_admin" {
    name = "monitoring-admin"
    group = aws_iam_group.sre-delta-squad.name

    policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
            {
                Action = [
                    "cloudwatch:DescribeInsightRules",
                    "cloudwatch:DescribeAlarmHistory",
                    "cloudwatch:GetDashboard",
                    "cloudwatch:GetInsightRuleReport",
                    "cloudwatch:GetMetricData",
                    "cloudwatch:DescribeAlarmsForMetric",
                    "cloudwatch:DescribeAlarms",
                    "cloudwatch:GetMetricStream",
                    "cloudwatch:GetMetricStatistics",
                    "cloudwatch:GetMetricWidgetImage",
                    "cloudwatch:DescribeAnomalyDetectors"
                ]
                Effect   = "Allow"
                Resource = "*"
            }
        ]
    })
}

# Admin access to all clusters
resource "aws_iam_group_policy" "eks_admin" {
    name = "eks-admin"
    group = aws_iam_group.developers.name

    policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
            {
                Action = ["eks:*"]
                Effect = "Allow"
                Resource = "*"
            }
        ]
    })
}

# Read access to S3 (Marketing Materials)
# All resources for simplicity of the demo.
resource "aws_iam_group_policy" "s3_grab" {
    name = "s3_grab"
    group = aws_iam_group.marketing-echo-squad.name

    policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
            {
                Action = [
                    "s3:GetObject",
                ]
                Effect = "Allow"
                Resource = "*"
            }
        ]
    })
}


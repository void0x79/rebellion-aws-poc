##### SRE Engineers ######
resource "aws_iam_group" "sre-delta-squad" {
    name = "sre-delta"
}

resource "aws_iam_user" "eric-sre" {
    name = "eric-sre"
}

resource "aws_iam_user" "ralph-sre" {
    name = "ralph-sre"
}

##### Marketing ######
resource "aws_iam_group" "marketing-echo-squad" {
    name = "marketing-echo"
}

resource "aws_iam_user" "isiah-marketing" {
    name = "isiah-marketing"
}

##### Cloud Security Engineers #####
resource "aws_iam_group" "cloud-sec" {
    name = "cloud-sec"
}

resource "aws_iam_user" "sara-cloudsec" {
    name = "sara-cloudsec"
}

resource "aws_iam_user" "dallas-cloudsec" {
    name = "dallas-cloudsec"
}

####### developers ######
resource "aws_iam_group" "developers" {
    name = "developers"
}

resource "aws_iam_group" "rachel-dev" {
    name = "rachel-dev"
}

####### Vendors #####
resource "aws_iam_group" "vendors" {
    name = "vendors"
}

resource "aws_iam_user" "datadog-vendor" {
    name = "datadog-vendor"
}



# The provider for the rebellion demo
terraform {
    required_providers {
        aws = {
            source  = "hashicorp/aws"
            version = "4.18.0"
        }
    }
    
    backend "remote" {
        hostname = "app.terraform.io"
        organization = "rebellion-aws-poc"
        
        workspaces {
            name = "rebellion-aws-poc"
        }
    }
}

provider "aws" {
    region = "us-east-1"
}

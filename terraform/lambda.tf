# Log lambda function via cloudwatch for a 90 compliance requirement.

resource "aws_cloudwatch_log_group" "lambda-logger-group" {
    name      = "/aws/rebellion/${aws_lambda_function.rebellion-func01.function_name}"
    retention_in_days = 90
}


# This isn't going to work b/c I don't have any source code to upload
# plus $$$ LOL 
// resource "aws_lambda_function" "rebellion-func01" {
//     function_name = "rebellion-aws-poc-func"
//     role          = "aws_iam_role.lambda-executor.arn"
//     handler       = "index.test"
//     runtime       = "python3.9"
//     s3_bucket     = aws_s3_bucket.rebel-yell.id
// }
